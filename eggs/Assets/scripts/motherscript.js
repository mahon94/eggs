﻿#pragma strict
var speed : int = 3;
var redEggPre : Transform;
var blueEggPre : Transform;
var goldEggPre : Transform;
var bombEggPre : Transform;
var eggRate : float = 1f;
var randomRate : float = .3f;
var lastEgg : float ;
var manager : GameObject;
var numSpeed : float = 2f;
var startTour : float = 0.06;
var endTour : float = 0.94;

function Start () {
	lastEgg = Time.time;
	manager = GameObject.FindGameObjectsWithTag("manager")[0];
	manager.GetComponent(manager_script).adjustTour();

}

function Update () {
var isFreez : float  = PlayerPrefs.GetFloat("freez");
		
	if(isFreez<=0){
	manager = GameObject.FindGameObjectsWithTag("manager")[0];
	numSpeed = manager.GetComponent(manager_script).motherSpeed;
	var minEggRate :float = (8.0f - numSpeed)/10.0f;
	 var  pos : Vector3 = Camera.main.WorldToViewportPoint(transform.position);
	 if (pos.x <= startTour && speed > 0)
	 	speed *=-1;
	
	 if (pos.x >= endTour && speed < 0)
	 	speed *=-1;
	 
	 if (speed > 0 && numSpeed < 0 || speed < 0 && numSpeed > 0 )
	 	speed = numSpeed * -1;
	 
	 transform.Translate(Vector3(speed*Time.deltaTime,0,0));
	 	
	 if (Random.Range(0.0,1.0)>=randomRate && lastEgg <= Time.time){
	 	eggRate=Random.Range(minEggRate,1);
	 	lastEgg = Time.time+eggRate;
		
	 if(PlayerPrefs.GetInt("firstPlay")==100){
		 if(Random.Range(0.0,1)>0.93)
			Instantiate(bombEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));
		else if(Random.Range(0.0,1)>0.90)
			Instantiate(goldEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));
	 	else if(Random.Range(0.0,1)>0.45)
		 	Instantiate(redEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));
		else
			Instantiate(blueEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));
		
			
	 }
	 else if(PlayerPrefs.GetInt("firstPlay")<50){
	  	Instantiate(redEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));	 
	 }
	 else if(PlayerPrefs.GetInt("firstPlay")<100){
	  	Instantiate(blueEggPre,Vector3(transform.position.x,transform.position.y-1,-2),Quaternion(0,45,0,0));	 
	 }
	 
	}
}
}


function setTour(start , end){
	startTour=start;
	endTour=end;
}