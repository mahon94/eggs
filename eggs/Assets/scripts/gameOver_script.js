﻿#pragma strict
var start : float = 0;
var end : float = 1.2;
var scoreNum : int = 0 ;
public var score : TextMesh;

function Start () {
	scoreNum = PlayerPrefs.GetInt("highScore");
	score.text = scoreNum.ToString();
}

function Update () {
	start += Time.deltaTime;
	if (start>=end)
		Application.LoadLevel(1);
}
