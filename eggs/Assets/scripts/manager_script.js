﻿#pragma strict

var color : int = 0;
//var level_text : TextMesh;
var touchBegins = {};
var mother : Transform;
var omlet : Transform;
var juje : Transform;
var smoke : Transform;
var background : Transform;
var level_object : Transform;
var life_object : TextMesh;
var color_vasat : Transform;
var noorVasat : Light;

var life : int = 3;
var level : int = 1;
var boost : int = 0;
var maxBoost : int = 10;
var eggSpeed : int = -2;
var inLoadNext : float = 0;
var playerLives : int = 3;
var scorekeeper : scorescript;
var maxMum : int = 3; 
var mumNum : int = 1;
var motherSpeedDefault : float = 3.0f;
var motherSpeed : float = 3.0f;
var motherSpeedMax : float = 6.0f;
var cameraUp : Vector3;
var cameraDown : Vector3;
var goUp : float;
var levelCount : float = 10.0;
var getEggs : int = 0;

public var tut1 : TextMesh;


private var hit: RaycastHit;
function makeJuje( posi ){
	if (mumNum < maxMum){
			Instantiate(juje, posi, Quaternion.identity);
			mumNum ++;
			motherSpeed = motherSpeedDefault;
	}
}

function gameOver(){
	scorekeeper.setHighScore();
	Application.LoadLevel(2);
}

function decLife(){
	life--;
	if (life <= 0)
		gameOver();	
	life_object.text = life.ToString();
		
}

function loadNextLevel(){


	var moms = GameObject.FindGameObjectsWithTag("mom");

	for (var mom in moms){
		Destroy(mom.gameObject);
	}
	
	var jujes = GameObject.FindGameObjectsWithTag("juje");

	for (var juje in jujes){
		Destroy(juje.gameObject);
	}
	
	var eggs = GameObject.FindGameObjectsWithTag("redEgg");

	for (var egg in eggs){
		Destroy(egg.gameObject);
	}
	eggs = GameObject.FindGameObjectsWithTag("blueEgg");

	for (var egg in eggs){
		Destroy(egg.gameObject);
	}
	level_object.GetComponent(TextMesh).text = "Level " + level.ToString() + " completed!";
	Instantiate(level_object, Vector3(0,4,-5),Quaternion.identity);
	var angle : float = 360/levelCount;
	var startAngle : float = 0;
	getEggs = 0;
	var x : float;
	var y : float;
	var r : float = 2.4;
	for ( var i = 0; i < levelCount ; i++){
		x = r*Mathf.Cos((Mathf.PI / 180)* startAngle);
		y = r*Mathf.Sin((Mathf.PI / 180)* startAngle);
		startAngle += angle;
		Instantiate(omlet, Vector3(x,y,-1),Quaternion.identity);
	}

	inLoadNext = 3;
	background.gameObject.transform.position=Vector3(0,-13,-5);
	level++;
	eggSpeed-=0.5;
	motherSpeed += 0.2;
	motherSpeedDefault += 0.2;
	motherSpeedMax += 0.2;
	levelCount = levelCount * 0.2 + levelCount;
	boost = 0;
	//tut1.text="sweep to change color";
	Debug.Log(tut1.text);
	/////////////////////////////////
	if(PlayerPrefs.GetInt("firstPlay")<50){////////1.just red eggs
	
	
	tut1.text="sweep to change color";
		PlayerPrefs.SetInt("firstPlay",50);
		PlayerPrefs.Save();
		Debug.Log("firstplay is "+PlayerPrefs.GetInt("firstPlay"));
		
	}	
	else if(PlayerPrefs.GetInt("firstPlay")<100){////////2.just blue eggs
		PlayerPrefs.SetInt("firstPlay",100);
		PlayerPrefs.Save();
		Destroy(tut1.gameObject);
	}		
}

function decScore(){
	decLife();
	
	scorekeeper.DecScore();
	var countDown : int = -2;
	if ( getEggs == 1)
		countDown = -1;
	if (getEggs <= 0)
		countDown = 0;
	
	getEggs += countDown;

	boost = 0;
	
	background.gameObject.transform.Translate(Vector3(0,goUp*countDown,0));
}

function adjustTour(){
			var moms = GameObject.FindGameObjectsWithTag("mom");
			var sizeTour : float = 1.0f/mumNum;
			
			var s : float = 0;

			for (var mom in moms){
			mom.GetComponent(motherscript).setTour(s+0.04,s+sizeTour-0.04);
			s += sizeTour;
			}
}

function waitForJuje(){
	var eggs = GameObject.FindGameObjectsWithTag("redEgg");
	for (var egg in eggs){
		Destroy(egg.gameObject);
	}
	eggs = GameObject.FindGameObjectsWithTag("blueEgg");
	for (var egg in eggs){
		Destroy(egg.gameObject);
	}
}

function addScore (){
	if(getEggs>=levelCount){
		loadNextLevel();
		Debug.Log("level");
		return;
		}
	if (boost != maxBoost)
		boost++;
	
	scorekeeper.AddScore(boost);
	if (inLoadNext<=0){
		background.gameObject.transform.Translate(Vector3(0,goUp,0));
		getEggs++;
	}
//	Destroy(background.gameObject);
}

function findGoUp (){

	goUp = (cameraUp.y - cameraDown.y)/levelCount;
		if (goUp<0)
		goUp*=-1;

}

function Start () {	
	PlayerPrefs.SetInt("firstPlay",100);
	PlayerPrefs.SetFloat("freez",0);
	PlayerPrefs.Save();

	tut1.text="Destroy eggs";
	level = 1;
	life = 3;
	mumNum = 1;
	boost = 0;
	eggSpeed = -2;
	inLoadNext = 0;
	playerLives = 3;
	life_object.text = life.ToString();
	motherSpeedDefault = 3.0f;
	motherSpeed  = 3.0f;
	motherSpeedMax = 6.0f;
	levelCount  = 10.0;
	getEggs  = 0;
	
	Debug.Log(PlayerPrefs.GetInt("firstPlay"));
	
	Debug.Log("tttttttttttttttttttttttttttttttttttttttttttt");
	cameraUp = Camera.main.ViewportToWorldPoint(Vector3(0,0,0));
//	Debug.Log(cameraUp);
	cameraDown = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
//	Debug.Log(cameraDown);
	findGoUp();
	Instantiate(mother , Vector3(0,5,-1),Quaternion(0,45,0,0));
//	player = GameObject.FindGameObjectWithTag("player");
}

function resetLevel () {	
	mumNum = 1;
	boost = 0;
	cameraUp = Camera.main.ViewportToWorldPoint(Vector3(0,0,0));
//	Debug.Log(cameraUp);
	cameraDown = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
//	Debug.Log(cameraDown);
	findGoUp();
	Instantiate(mother , Vector3(0,5,-1),Quaternion(0,45,0,0));
//	player = GameObject.FindGameObjectWithTag("player");
}

function Update () {
	var isFreez : float  = PlayerPrefs.GetFloat("freez");
	if(isFreez>0){
		isFreez-= Time.deltaTime;
		PlayerPrefs.SetFloat("freez",isFreez);
		PlayerPrefs.Save();
		}
/*	if (player == null && playerLives > 0){
		Instantiate(playerShip , Vector3(0,-2,2),Quaternion.identity);
		player = GameObject.FindGameObjectWithTag("player");
		playerLives--;
	}
*/	
	if ( inLoadNext > 0){
		inLoadNext -= Time.deltaTime;
		}
	if (motherSpeed < motherSpeedMax)
		motherSpeed += 0.01 * Time.deltaTime;
	for (var i = 0; i < Input.touchCount; ++i) 
	{
	var touchDeltaPosition:Vector2 = Input.GetTouch(0).deltaPosition;
	
		if (Input.GetTouch(i).phase == TouchPhase.Began){
			var ray = Camera.main.ScreenPointToRay (Input.GetTouch(i).position);
			touchBegins[Input.GetTouch(i).fingerId] = Input.GetTouch(i).position;
			

			var start : Quaternion;
			start = new Quaternion(0.0, 0.0, 0.0, 0.0);
			start.SetEulerRotation(180,0,0);
			if(Physics.Raycast (ray,hit,Mathf.Infinity)){
			if (hit.collider.tag == "terekan"){
			
				var jujes = GameObject.FindGameObjectsWithTag("juje");

				for (var juje in jujes){
					Destroy(juje.gameObject);
				}
				
				var eggs = GameObject.FindGameObjectsWithTag("redEgg");

				for (var egg in eggs){
					Destroy(egg.gameObject);
				}
				eggs = GameObject.FindGameObjectsWithTag("blueEgg");

				for (var egg in eggs){
					Destroy(egg.gameObject);
				}
				
				eggs = GameObject.FindGameObjectsWithTag("bomb");

				for (var egg in eggs){
					Destroy(egg.gameObject);
				}
				
				eggs = GameObject.FindGameObjectsWithTag("terekan");

				for (var egg in eggs){
					Destroy(egg.gameObject);
				}
						
			}
			if (hit.collider.tag == "bomb"){
			gameOver();	
			}
				if (hit.collider.tag == "redEgg"){
					Destroy(hit.collider.gameObject);
					if (color == 1){

					Instantiate(smoke,hit.collider.transform.position,start);
					decScore();
					
					
					}
					else{
						Instantiate(omlet,hit.collider.transform.position,Quaternion.identity);
					
						addScore();
						
					}
				}
				if ( hit.collider.tag == "blueEgg"){
						Destroy(hit.collider.gameObject);
					if(color == 1){
						Instantiate(omlet,hit.collider.transform.position,Quaternion.identity);
					

						addScore();
					}
					else{
						Instantiate(smoke,hit.collider.transform.position,start);//duuud
						decScore();
					}
				}

			
				if (hit.collider.tag == "changeCol" && color == 1 ){
					noorVasat.GetComponent.<Light>().color = new Color(1f,0.0f,0.0f);
					color_vasat.gameObject.GetComponent.<Renderer>().material.color = new Color(1f,0.3f,0.3f);
					hit.collider.gameObject.GetComponent.<Renderer>().material.color = new Color(1f,0.3f,0.3f);
					background.gameObject.GetComponent.<Renderer>().material.color = new Color(1f,0.3f,0.3f,0.4);
					
					color = 0;
					}
				else if (hit.collider.tag == "changeCol" && color == 0 ){
					noorVasat.GetComponent.<Light>().color  =  new Color(0.0f,0.0f,1f);
					color_vasat.gameObject.GetComponent.<Renderer>().material.color	= new Color(0.3f,0.3f,1f);
					hit.collider.gameObject.GetComponent.<Renderer>().material.color = new Color(0.3f,0.3f,1f);
					background.gameObject.GetComponent.<Renderer>().material.color = new Color(0.3f,0.3f,1f,0.4);
					color = 1;
					}
				else if (hit.collider.tag == "omlet" && inLoadNext>0){
					Destroy(hit.collider.gameObject);
					addScore();
				}
			}
		}
		
		if (Input.GetTouch(i).phase == TouchPhase.Ended){
		var position_Delta : Vector2= touchBegins[Input.GetTouch(i).fingerId];
		position_Delta = Input.GetTouch(i).position - position_Delta ;
			if((position_Delta.x > 50 && !(position_Delta.y > 30 || position_Delta.y < -30)) || (position_Delta.x < -50 &&!(position_Delta.y > 30 || position_Delta.y < -30))) {
					if ( color == 1 ){
					noorVasat.GetComponent.<Light>().color = new Color(1f,0.0f,0.0f);
					color_vasat.gameObject.GetComponent.<Renderer>().material.color = new Color(1f,0.3f,0.3f);
					background.gameObject.GetComponent.<Renderer>().material.color = new Color(1f,0.3f,0.3f,0.4);
					color = 0;
					}
				else if (color == 0 ){
					noorVasat.GetComponent.<Light>().color  =  new Color(0.0f,0.0f,1f);
					color_vasat.gameObject.GetComponent.<Renderer>().material.color	= new Color(0.3f,0.3f,1f);
					background.gameObject.GetComponent.<Renderer>().material.color = new Color(0.3f,0.3f,1f,0.4);
					color = 1;
					}
			}
			if((position_Delta.y > 30 || position_Delta.y < -30)&& !(position_Delta.x > 50 || position_Delta.x < -50)){
					Debug.Log("in \freez");
					PlayerPrefs.SetFloat("freez",2);
					PlayerPrefs.Save();
			}
		}

	}
}
